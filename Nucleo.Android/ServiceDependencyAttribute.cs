using System;

namespace Nucleo
{
	[AttributeUsage(AttributeTargets.All)]
	public class ServiceDependencyAttribute : System.Attribute
	{
		public ServiceDependencyAttribute ()
		{
		}
	}
}

