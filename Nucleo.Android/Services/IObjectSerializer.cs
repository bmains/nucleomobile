using System;
using System.Collections.Generic;
using System.Linq;


namespace Nucleo.Services
{
	public interface IObjectSerializer
	{

		T DeserializeObject<T>(string content);

		string SerializeObject<T>(T obj);

	}
}

