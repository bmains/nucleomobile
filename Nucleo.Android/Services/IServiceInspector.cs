using System;
using System.Collections.Generic;


namespace Nucleo.Services
{
	public interface IServiceInspector
	{

		IEnumerable<Type> FindServiceTypes();

	}
}

