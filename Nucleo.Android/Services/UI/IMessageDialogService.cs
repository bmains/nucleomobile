using System;


namespace Nucleo.Services.UI
{
	public interface IMessageDialogService
	{

		void Show(object source, string title, string message);

	}
}

