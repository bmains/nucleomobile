using System;
using Android.App;
using Android.Widget;
using Android.Content;


namespace Nucleo.Services.UI
{
	[ServiceImplementation]
	public class AndroidDialogMessageService : IMessageDialogService
	{
		public AndroidDialogMessageService ()
		{
		}


		public void Show (object source, string title, string message)
		{
			Context ctx = null;

			if (source is Activity) {
				ctx = ((Activity)source).ApplicationContext;
			}
			else if (source is Context) {
				ctx = (Context)source;
			}

			Toast.MakeText (ctx, message, ToastLength.Short).Show ();
		}

	}
}

