using System;
using Newtonsoft.Json;


namespace Nucleo.Services
{
	[ServiceImplementation]
	public class JsonObjectSerializer : IObjectSerializer
	{

		public JsonObjectSerializer ()
		{
		}



		public T DeserializeObject<T> (string content)
		{
			return JsonConvert.DeserializeObject<T> (content);
		}

		public string SerializeObject<T> (T obj)
		{
			return JsonConvert.SerializeObject (obj);
		}

	}
}

