using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime;


namespace Nucleo.Services
{

	public class AssemblyAttributeServiceInspector : IServiceInspector
	{


		public AssemblyAttributeServiceInspector ()
		{
		}



		public IEnumerable<Type> FindServiceTypes()
		{
			var assemblies = AppDomain.CurrentDomain.GetAssemblies ();
			var output = new List<Type> ();

			foreach (var assembly in assemblies) {
				output.AddRange (assembly.GetTypes ().Where (i => i.GetCustomAttributes (typeof(ServiceImplementationAttribute), false) != null));
			}

			return output;
		}

	}
}

