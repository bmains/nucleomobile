using System;
using RestSharp;


namespace Nucleo.Services
{
	public interface IRestRequestBuilder
	{

		IRestRequest CreateRequest(string partialUrl, Method method);

		IRestResponse CreateResponse(IRestRequest request);

	}
}

