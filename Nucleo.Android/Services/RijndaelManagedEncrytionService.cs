using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;


namespace Nucleo.Services
{
	public class AppEncrytionService : IEncryptionService
	{
		private byte[] _key;
		private byte[] _iv;



		public AppEncrytionService (byte[] key, byte[] iv)
		{
			_key = key;
			_iv = iv;
		}



		public string Decrypt (string encryptedText)
		{
			string plaintext;

			using (RijndaelManaged rijAlg = new RijndaelManaged())
			{
				rijAlg.Key = _key;
				rijAlg.IV = _iv;

				var decryptor = rijAlg.CreateDecryptor (rijAlg.Key, rijAlg.IV);
			
				using (MemoryStream msDecrypt = new MemoryStream(Convert.FromBase64String (encryptedText)))
				{
					using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
					{
						using (StreamReader srDecrypt = new StreamReader(csDecrypt))
						{
							// Read the decrypted bytes from the decrypting stream 
							// and place them in a string.
							plaintext = srDecrypt.ReadToEnd();
						}
					}
				}
			}

			return plaintext;
		}

		public string Encrypt (string decryptedText)
		{
			byte[] encrypted;

			// Create an RijndaelManaged object 
			// with the specified key and IV. 
			using (RijndaelManaged rijAlg = new RijndaelManaged())
			{
				rijAlg.Key = _key;
				rijAlg.IV = _iv;

				// Create a decrytor to perform the stream transform.
				ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

				// Create the streams used for encryption. 
				using (MemoryStream msEncrypt = new MemoryStream())
				{
					using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
					{
						using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
						{

							//Write all data to the stream.
							swEncrypt.Write(decryptedText);
						}
						encrypted = msEncrypt.ToArray();
					}
				}
			}


			// Return the encrypted bytes from the memory stream. 
			return Convert.ToBase64String (encrypted);
		}

		public string Hash (string unhashedText)
		{
			byte[] hashBytes = Encoding.Unicode.GetBytes (unhashedText);

			var provider = new SHA1CryptoServiceProvider ();
			byte[] output = provider.ComputeHash (hashBytes);

			return Convert.ToBase64String (output);
		}
	}
}

