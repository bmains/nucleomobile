using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace Nucleo.UI
{
	public abstract class BaseListAdapter<T> : Android.Widget.BaseAdapter
	{
		private Activity _activity = null;
		private IList<T> _data = null;
		private int _resource = 0;



		public override int Count {
			get {
				return _data.Count;
			}
		}

		public override int ViewTypeCount {
			get {
				return base.ViewTypeCount;
			}
		}



		public BaseListAdapter (IList<T> data, Activity activity)
			: base()
		{
			_activity = activity;
			_data = data;
		}

	

		public void AddItem(T item)
		{
			_data.Add (item);
			this.NotifyDataSetChanged ();
		}

		public override Java.Lang.Object GetItem (int position)
		{
			var obj = (object)_data [position];
			return (Java.Lang.Object)obj;
		}

		public override long GetItemId (int position)
		{
			return position;
		}

		protected abstract int GetResourceID (int type);

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View view = convertView;
			var type = this.GetItemViewType (position);

			if (view == null) {
				view = _activity.LayoutInflater.Inflate (this.GetResourceID (type), null);
			}

			this.PopulateView (view, _data [position], type);

			return view;
		}

		public override int GetItemViewType (int position)
		{
			return base.GetItemViewType (position);
		}

		public override void NotifyDataSetChanged ()
		{
			base.NotifyDataSetChanged ();
		}

		protected abstract void PopulateView (View view, T data, int type);

		public void RemoveItem(T item)
		{
			_data.Remove (item);
			this.NotifyDataSetChanged ();
		}

	}
}

