using System;

namespace Nucleo
{
	public class DataEventArgs<T>
	{
		public T Data { get; set; }



		public DataEventArgs() { }

		public DataEventArgs (T data)
		{
			Data = data;
		}
	}


	public delegate void DataEventHandler<T>(object sender, DataEventArgs<T> e);
}

