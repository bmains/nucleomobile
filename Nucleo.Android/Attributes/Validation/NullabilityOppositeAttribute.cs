using System;
using System.ComponentModel.DataAnnotations;


namespace Nucleo.Attributes.Validation
{
	/// <summary>
	/// Compares the related property to ensure either this value, or the other value, is null.
	/// </summary>
	public class NullabilityOppositeAttribute : ValidationAttribute 
	{

		public string ComparisonProperty
		{
			get;
			set;
		}



		private bool CheckNullOrEmpty(object value)
		{
			bool isNullOrEmpty = (value == null);
			if (isNullOrEmpty == false) {
				isNullOrEmpty = (value is string && string.IsNullOrEmpty (value.ToString ()));
			}

			return isNullOrEmpty;
		}

		protected override ValidationResult IsValid (object value, ValidationContext context)
		{
			if (string.IsNullOrEmpty (this.ComparisonProperty))
				return null;

			bool isNullOrEmpty = CheckNullOrEmpty (value);

			var otherProp = context.ObjectType.GetProperty (this.ComparisonProperty);
			bool otherIsNullOrEmpty = CheckNullOrEmpty(otherProp.GetValue (context.ObjectInstance, null));

			if (isNullOrEmpty == otherIsNullOrEmpty)
				return new ValidationResult (ErrorMessage ?? "Both " + context.MemberName + " and " + ComparisonProperty + " must be null.");
			else
				return null;
		}

	}

}

