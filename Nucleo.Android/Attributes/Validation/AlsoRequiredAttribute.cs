using System;
using System.ComponentModel.DataAnnotations;


namespace Nucleo
{
	public class AlsoRequiredAttribute : ValidationAttribute
	{
		public string ComparisonProperty
		{
			get;
			set;
		}



		protected override ValidationResult IsValid (object value, ValidationContext context)
		{
			var prop = context.ObjectType.GetProperty (this.ComparisonProperty);
			object val = prop.GetValue (context.ObjectInstance, null);

			if (val != null) {
				if (val is string) {
					if (!string.IsNullOrEmpty (val.ToString ()))
						return null;
				} 
				else
					return null;
			}

			return new ValidationResult(ErrorMessage ?? ComparisonProperty + " is also required.");
		}

	}
}

