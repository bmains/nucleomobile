using System;

namespace Nucleo.Services
{
	public class SqliteDatabaseService : ISqliteDatabaseService
	{
		private string _databasePath = null;



		public SqliteDatabaseService (string databasePath)
		{
			_databasePath = databasePath;
		}



		public virtual string GetDefaultDatabasePath ()
		{
			return _databasePath;
		}

		public virtual SQLite.SQLiteConnection OpenConnection ()
		{
			return new SQLite.SQLiteConnection (this.GetDefaultDatabasePath ());
		}

	}
}

