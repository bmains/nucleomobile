using System;


namespace Nucleo.Services
{
	public interface ISqliteDatabaseInitializer
	{

		void Initialize(string databaseName);

	}
}

