using System;
using SQLite;


namespace Nucleo.Data
{
	[Table("AppSetting")]
	public class AppSetting
	{
		public AppSetting ()
		{
		}


		[Column("Name")]
		[PrimaryKey]
		public string Name { get; set; }

		[Column("Value")]
		public string Value { get; set; }
	}
}

