using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Facebook;
using RestSharp;
using Nucleo.Services;
using Nucleo.Services.Social;
using Nucleo.Services.UI;


namespace Nucleo.Controllers
{
	/// <summary>
	/// Creates a web browser control that will navigate to facebook for authentication purposes.
	/// </summary>
	/// <remarks>
	/// Override the Configure
	/// </remarks>
	public class FacebookLoginController : BaseUIViewController
	{
		private IFacebookService _facebook = null;
		private IMessageDialogService _dialogs = null;
		private FacebookLoginDelegate _delegate = null;
		private UIWebView _browser = null;
		private string _appID = null;



		public UIWebView Browser
		{
			get { return _browser; }
		}

		public FacebookLoginDelegate Delegate {
			get;
			set;
		}

		public IMessageDialogService Dialogs
		{
			get { return _dialogs; }
		}

		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}



		public FacebookLoginController (IFacebookService facebook, IMessageDialogService dialogs)
			: base()
		{
			_facebook = facebook;
			_dialogs = dialogs;
		}


		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			if (this.Delegate == null)
				throw new ArgumentNullException ("Delegate");

			_browser = new UIWebView ();
			this.Delegate.ConfigureBrowser (_browser);
			this.View.AddSubview (_browser);

			string uri = _facebook.GetLoginUrl(this.Delegate.AppID, "publish_actions");
			NSUrl url = NSUrl.FromString(uri);
			NSUrlRequest request = NSUrlRequest.FromUrl (url);

			_browser.LoadRequest (request);

			// Perform any additional setup after loading the view, typically from a nib.
			_browser.LoadFinished += Browser_LoadFinished;
		}



		private void Browser_LoadFinished(object sender, EventArgs e)
		{
			var fb = new FacebookClient();

			FacebookOAuthResult result = null;

			try
			{
				result = fb.ParseOAuthCallbackUrl (new Uri(_browser.Request.Url.AbsoluteString));
			}
			catch
			{
				//Will throw exception everytime until URL is correct
				return;
			}

			try
			{
				if (result.IsSuccess)
				{
					fb.AccessToken = result.AccessToken;
					if (this.Delegate.LoginUser(result) == true)
						return;

					var controller = this.Delegate.RedirectOnLoginController();
					if (controller != null)
						this.NavigationController.PushViewController(controller, true);
					else
						throw new InvalidOperationException("Cannot redirect to a controller; the delegate is not configured correctly.");

				}
				else
				{
					var errorDescription = result.ErrorDescription;
					var errorReason = result.ErrorReason;
				}

			}
			catch (Exception ex) {
				this.Delegate.HandleError (ex);
			}
		}

	}

	public abstract class FacebookLoginDelegate
	{
		private FacebookLoginController _controller = null;


		public string AppID { get; set; }



		public FacebookLoginDelegate(FacebookLoginController controller)
		{
			_controller = controller;
		}



		public virtual void ConfigureBrowser(UIWebView browser)
		{
			_controller.Browser.Frame = _controller.View.Frame;
		}

		/// <summary>
		/// Logins the user.  This must return true if the delegate method handles the login; otherwise, it will then call <see cref="RedirectOnLoginController"/> method.  If that method returns null, an exception is thrown.
		/// </summary>
		/// <returns><c>true</c>, if user login result was processed, <c>false</c> otherwise.</returns>
		/// <param name="result">The authentication result from Facebook.</param>
		public virtual bool LoginUser(FacebookOAuthResult result)
		{
			return false;
		}

		public virtual void HandleError(Exception ex)
		{
			_controller.Dialogs.Show(this, "Error", "An error has occurred.");
		}

		/// <summary>
		/// Redirects to a specified controller after login.  Must be specified; otherwise, you must supply login code from <see cref="LoginUser"/>  and return true.  If none of these options are specified, an error is thrown.
		/// </summary>
		/// <returns>The controller to redirect to.</returns>
		public virtual UIViewController RedirectOnLoginController()
		{
			return null;
		}


	}
}

