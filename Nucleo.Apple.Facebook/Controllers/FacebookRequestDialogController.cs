using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Nucleo.Services.Social;



namespace Nucleo.Controllers
{
	public class FacebookRequestDialogController : DismissableModalController
	{
		private IFacebookDialogService _dialogs = null;
		private FacebookDialogOptions _options = null;
		private UIWebView _browser = null;
		private bool _configured = false;


		public event FacebookGameRequestEventHandler Completed;



		public FacebookRequestDialogController(IFacebookDialogService dialogs)
			: base() 
		{
			_dialogs = dialogs;
		}



		public void Configure(FacebookDialogOptions options)
		{
			if (options == null)
				throw new ArgumentNullException ("options");
			if (string.IsNullOrWhiteSpace (options.AppID))
				throw new ArgumentNullException ("options.AppID");
			if (string.IsNullOrWhiteSpace(options.Message))
				throw new ArgumentNullException ("options.Message");

			_options = options;
			_configured = true;
		}

		private NSUrlRequest GetUri()
		{
			if (!_configured)
				throw new Exception ("The request dialog was not configured; call the Configure method first, especially before the ViewDidLoad method runs");

			string uri = _dialogs.GetDialogUrl (_options);
			NSUrl url = NSUrl.FromString(uri);
			return NSUrlRequest.FromUrl (url);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			_browser = new UIWebView ();
			_browser.Frame = this.View.Frame;

			this.View.AddSubview (_browser);

			NSUrlRequest request = GetUri ();
			_browser.LoadRequest (request);

			// Perform any additional setup after loading the view, typically from a nib.
			_browser.LoadFinished += Browser_LoadFinished;
		}

		protected virtual void OnCompleted(FacebookGameRequestEventArgs e)
		{
			if (Completed != null)
				Completed (this, e);
		}


		void Browser_LoadFinished (object sender, EventArgs e)
		{
			var query = _browser.Request.Url.Query;

			var parms = _dialogs.ExtractRequestParameters (query);
			this.OnCompleted (new FacebookGameRequestEventArgs { Request = parms.RequestID, To = parms.To });
		}
	}


	public class FacebookGameRequestEventArgs
	{

		public string Request { get; set; }

		public string To { get; set; }

	}

	public delegate void FacebookGameRequestEventHandler(object sender, FacebookGameRequestEventArgs e);

}
