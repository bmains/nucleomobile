using System;

namespace Nucleo
{
	public class DateTimeUtilities
	{

		/// <summary>
		/// Displays intuitive text about the date and time, such as X minutes ago, X hours ago, or simply the date.
		/// </summary>
		/// <returns>The intuitive text.</returns>
		/// <param name="date">Date.</param>
		public static string DisplayIntuitiveText(DateTime? date)
		{
			if (!date.HasValue)
				return "Unknown";

			var time = DateTime.Now.Subtract (date.Value);

			if (time.TotalDays >= 3)
				return string.Format ("{0}/{1}/{2}", date.Value.Month, date.Value.Day, date.Value.Year);
			else if (time.TotalDays > 1)
				return Convert.ToInt32 (Math.Floor (time.TotalDays)).ToString () + " days ago";
			else if (time.TotalDays == 1)
				return "1 day ago";

			if (time.TotalHours < 1) {
				if (time.TotalMinutes > 1)
					return Convert.ToInt32 (Math.Floor (time.TotalMinutes)).ToString () + " minutes ago";
				else
					return "1 minute ago";
			} 
			else {
				if (time.TotalHours > 1)
					return Convert.ToInt32 (Math.Floor (time.TotalHours)).ToString () + " hours ago";
				else
					return "1 hour ago";
			}
		}

	}
}

