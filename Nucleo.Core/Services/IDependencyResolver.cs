using System;
using System.Collections.Generic;


namespace Nucleo.Services
{
	public interface IDependencyResolver
	{
		IEnumerable<TInt> GetAllInstances<TInt>() 
			where TInt: class;

		IEnumerable<object> GetAllInstances (Type tint);

		TInt GetInstance<TInt> ()
			where TInt: class;

		object GetInstance(Type type);

	}
}

