using System;

namespace Nucleo.Services
{
	public interface IEncryptionService
	{

		string Decrypt(string encryptedText);

		string Encrypt(string decryptedText);

		string Hash(string unhashedText);

	}
}

