using System;


namespace Nucleo.Services
{
	public interface IAppSettingService
	{

		string GetSetting(string name);

		void SetSetting(string name, string value);

	}
}

