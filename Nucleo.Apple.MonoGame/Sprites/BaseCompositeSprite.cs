using System;
using System.Linq;
using System.Collections.Generic;


namespace Nucleo.MonoGame
{
	public class BaseCompositeSprite : BaseSprite
	{
		private List<BaseSprite> _sprites = null;



		public List<BaseSprite> Sprites
		{
			get {
				if (_sprites == null)
					_sprites = new List<BaseSprite> ();
				return _sprites;
			}
		}



		public BaseCompositeSprite ()
		{
		}


		public override void Draw (Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
		{
			foreach (var sprite in this.Sprites) {
				sprite.Draw (batch);
			}
		}

		public override void Initialize (InitializationOptions options)
		{
			foreach (var sprite in this.Sprites) {
				sprite.Initialize (options);
			}
		}

		public override bool IsInBounds (Microsoft.Xna.Framework.Vector2 point)
		{
			return this.Sprites.Any (i => i.IsInBounds (point));
		}

		public override void Update (SpriteUpdateOptions options)
		{
			foreach (var sprite in this.Sprites) {
				sprite.Update (options);
			}
		}

	}
}

