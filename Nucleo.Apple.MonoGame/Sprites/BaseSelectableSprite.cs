using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Nucleo.MonoGame
{
	public abstract class BaseSelectableSprite : BaseSprite, ISelectableSprite
	{

		public bool IsSelected { get; private set; }

		public Texture2D SelectedTexture { get; set; }



		public BaseSelectableSprite ()
		{
		}



		public virtual void Deselect()
		{
			this.IsSelected = false;
		}

		protected override Texture2D GetTexture ()
		{
			return (this.IsSelected) ? this.SelectedTexture : this.Texture;
		}

		public virtual void Select(Vector2 position)
		{
			this.IsSelected = true;
		}

	}
}

