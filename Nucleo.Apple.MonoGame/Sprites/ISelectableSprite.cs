using System;
using Microsoft.Xna.Framework;


namespace Nucleo.MonoGame
{
	public interface ISelectableSprite
	{

		bool IsSelected { get; }

		void Deselect();

		void Select (Vector2 position);

	}
}

