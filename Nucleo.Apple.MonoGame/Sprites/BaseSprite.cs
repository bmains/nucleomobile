using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using System.Drawing;


namespace Nucleo.MonoGame
{
	public abstract class BaseSprite
	{

		public Microsoft.Xna.Framework.Color? Color { get; set; }

		public virtual int Height
		{
			get { return this.GetTexture ().Height; }
		}

		public Vector2 Position { get; set; }

		public Texture2D Texture { get; set; }

		public Vector2 Velocity { get; set; }

		public virtual int Width
		{
			get { return this.GetTexture ().Width; }
		}



		public BaseSprite ()
		{

		}


		//Framework methods

		public virtual void Initialize (InitializationOptions options) { }

		public virtual void UnloadContent() { }


		//Drawing methods

		public virtual void Draw (SpriteBatch batch)
		{
			batch.Draw (this.GetTexture (), this.Position, (Color.HasValue ? Color.Value : Microsoft.Xna.Framework.Color.White));
		}

		protected virtual Texture2D GetTexture ()
		{
			return this.Texture;
		}

		public virtual bool IsInBounds(Vector2 point)
		{
			return new RectangleF (this.Position.X, this.Position.Y, this.Width, this.Height).Contains (point.X, point.Y);
		}

		public abstract void Update (SpriteUpdateOptions options);

	}
}

