using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using System.IO;

using Nucleo.MonoGame;
using Microsoft.Xna.Framework.Graphics;


namespace Nucleo.MonoGame
{
	/// <summary>
	/// Enum describes the screen transition state.
	/// </summary>
	public enum ScreenState
	{
		TransitionOn,
		Active,
		TransitionOff,
		Hidden,
	}


	public class BaseScreen
	{
		private ScreenState _screenState = ScreenState.Active;
		private List<BaseSprite> _sprites = null;



		/// <summary>
		/// Gets the current screen transition state.
		/// </summary>
		public ScreenState ScreenState
		{
			get { return _screenState; }
			protected set { _screenState = value; }
		}

		public List<BaseSprite> Sprites
		{
			get
			{
				if (_sprites == null)
					_sprites = new List<BaseSprite>();
				return _sprites;
			}
		}




		public void Draw(GameTime gameTime, SpriteBatch batch)
		{
			batch.Begin ();

			foreach (var sprite in Sprites)
				sprite.Draw (batch);

			batch.End ();
		}

		public void Initialize (InitializationOptions options)
		{
			this.InitializeSprites ();

			foreach (var sprite in Sprites)
				sprite.Initialize (options);
		}

		protected virtual void InitializeSprites()
		{

		}

		protected void UnloadContent ()
		{
			foreach (var sprite in Sprites) {
				sprite.UnloadContent ();
			}
		}

		public void Update (GameTime gameTime, Rectangle window)
		{
			var options = new SpriteUpdateOptions (
				              gameTime,
							  window
			              );

			foreach (var sprite in Sprites) {
				sprite.Update (options);
			}
		}
	}
}

