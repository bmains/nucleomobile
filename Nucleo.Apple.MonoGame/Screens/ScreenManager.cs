using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Nucleo.MonoGame
{
	public class ScreenManager : DrawableGameComponent
	{
		public BaseScreen CurrentScreen 
		{
			get;
			private set;
		}


		public ScreenManager (Game game)
			:base(game)
		{

		}



		public void TransitionToScreen(BaseScreen screen)
		{
			var options = new InitializationOptions(
				this.GraphicsDevice,
				Game.Content,
				this.Game.Window.ClientBounds);

			screen.Initialize (options);

			this.CurrentScreen = screen;
		}



		public void Draw(GameTime gameTime, SpriteBatch batch)
		{
			this.Draw (gameTime);

			if (this.CurrentScreen != null)
				this.CurrentScreen.Draw (gameTime, batch);
		}

		public override void Draw (GameTime gameTime)
		{
			base.Draw (gameTime);
		}

		public void Update(GameTime gameTime)
		{
			if (this.CurrentScreen != null)
				this.CurrentScreen.Update (gameTime, Game.Window.ClientBounds);
		}
	}
}

