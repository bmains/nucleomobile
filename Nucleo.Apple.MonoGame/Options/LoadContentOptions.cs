using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace Nucleo.MonoGame
{
	public class LoadContentOptions
	{
		public ContentManager Content { get; private set; }

		public GraphicsDevice Device { get; private set; }



		public LoadContentOptions(ContentManager content, GraphicsDevice device)
		{
			this.Content = content;
			this.Device = device;
		}
	}
}

