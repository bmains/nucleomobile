using System;
using Microsoft.Xna.Framework;


namespace Nucleo.MonoGame
{
	public class SpriteUpdateOptions
	{

		public GameTime Time { get; private set; }

		public Rectangle Window { get; private set; }



		public SpriteUpdateOptions(GameTime time, Rectangle window)
		{
			Time = time;
			Window = window;
		}

	}
}

