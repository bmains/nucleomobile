using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace Nucleo.MonoGame
{
	public class InitializationOptions
	{
		public ContentManager Content { get; private set; }

		public GraphicsDevice Device { get; private set; } 

		public Rectangle GameWindow { get; private set; }



		public InitializationOptions(GraphicsDevice device, ContentManager content, Rectangle gameWindow)
		{
			this.Device = device;
			this.Content = content;
			this.GameWindow = gameWindow;
		}
	}
}

