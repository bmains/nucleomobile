using System;
using Android.Dialog;


namespace Nucleo.UI
{
	public class BusinessEntryElement : EntryElement, IBusinessItem
	{

		string IBusinessItem.Caption
		{
			get { return this.Caption; }
			set { this.Caption = value; }
		}

		public string FieldName
		{
			get;
			set;
		}




		public BusinessEntryElement (string caption, string fieldName)
			: base(caption, "")
		{
			this.FieldName = fieldName;
		}

		public BusinessEntryElement (string caption, string value, string fieldName)
			: base(caption, value)
		{
			this.FieldName = fieldName;
		}

		public BusinessEntryElement (string caption, string value, string fieldName, int layoutID)
			: base(caption, value, layoutID)
		{
			this.FieldName = fieldName;
		}




	}
}

