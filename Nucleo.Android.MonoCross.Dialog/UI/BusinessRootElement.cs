﻿using System;
using Android.Dialog;
using Android.Views;


namespace Nucleo.UI
{
	public class BusinessRootElement : RootElement
	{

		public BusinessRootElement (string caption)
			: base(caption)
		{
		}

		public BusinessRootElement (string caption, int section, int element)
			: base(caption, section, element)
		{
		}

		public BusinessRootElement (string caption, Group group)
			: base(caption, group)
		{
		}

		public BusinessRootElement (string caption, Func<RootElement, View> createOnSelected)
			: base(caption, createOnSelected)
		{
		}



	}
}

