﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Dialog;
using Android.Views;


namespace Nucleo.UI
{
	public class BusinessSection : Section
	{

		public BusinessSection () 
			: base()
		{
		}

		public BusinessSection(string caption)
			: base(caption) 
		{
		}

		public BusinessSection(Element header)
			: base(header)
		{
		}

		public BusinessSection(string caption, string footer)
			: base(caption, footer)
		{
		}

		public BusinessSection(object header, object footer)
			: base(header, footer)
		{
		}



		public IBusinessItem GetByField(string fieldName)
		{
			return this.Elements.OfType<IBusinessItem> ().FirstOrDefault (i => i.FieldName == fieldName);
		}

		public int? GetIndexPath (string fieldName)
		{
			for (int i = 0, len = this.Elements.Count; i < len; i++) {
				var el = this.Elements [i];

				if (el is IBusinessItem && ((IBusinessItem)el).FieldName == fieldName) {
					return i;
				}
			}

			return null;
		}

		public string GetValueOrDefault(string fieldName, string defaultValue)
		{
			var field = this.GetByField (fieldName);
			if (field != null)
				return field.Value;
			else
				return defaultValue;
		}

		public void Update(string fieldName, string value)
		{
			var indexPath = this.GetIndexPath (fieldName);

			if (indexPath != null) {
				((IBusinessItem)this.Elements [indexPath.Value]).Value = value;
			}
		}

	}
}

