﻿using System;
using Android.Dialog;


namespace Nucleo.UI
{
	public class BusinessLabelElement : StringElement, IBusinessItem
	{

		string IBusinessItem.Caption
		{
			get { return this.Caption; }
			set { this.Caption = value; }
		}

		public string FieldName
		{
			get;
			set;
		}

		string IBusinessItem.FieldName
		{
			get { return this.FieldName; }
			set { this.FieldName = value; }
		}

		string IBusinessItem.Value
		{
			get { return this.Value; }
			set { this.Value = value; }
		}



		public BusinessLabelElement (string caption, string fieldName)
			: base(caption, "") 
		{
			this.FieldName = fieldName;
		}

		public BusinessLabelElement (string caption, string value, string fieldName)
			: this(caption, value)
		{
			this.FieldName = fieldName;
		}

		public BusinessLabelElement (string caption, string value, string fieldName, int layoutID)
			: base(caption, value, layoutID)
		{
			this.FieldName = fieldName;
		}

	}
}

