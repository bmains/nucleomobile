﻿using System;


namespace Nucleo.UI
{
	public interface IBusinessItem
	{

		string Caption { get; set; }

		string FieldName { get; set; }

		string Value { get; set; }

	}
}

