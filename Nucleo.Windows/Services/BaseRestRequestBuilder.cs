using System;
using RestSharp;
using Newtonsoft.Json;



namespace Nucleo.Services
{
	public abstract class BaseRestRequestBuilder : IRestRequestBuilder
	{
		public BaseRestRequestBuilder ()
		{
		
		}



		public IRestRequest CreateRequest (string partialUrl, Method method)
		{
			var request = new RestRequest (partialUrl, method);
			this.SetupRequest (request);
			request.AddParameter ("response", "json");

			return request;
		}

		public IRestResponse CreateResponse (IRestRequest request)
		{
			if (request == null)
				throw new ArgumentNullException("request");

			var client = new RestClient (this.GetBaseUrl());
			var response = client.Execute (request);

			this.TransformResponse (response);

			return response;
		}

		protected abstract string GetBaseUrl ();

		protected abstract void SetupRequest(IRestRequest request);

		protected virtual IRestResponse TransformResponse (IRestResponse response)
		{
			return response;
		}
	}
}

