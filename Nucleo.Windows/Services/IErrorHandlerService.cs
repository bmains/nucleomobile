using System;


namespace Nucleo.Services
{
	public interface IErrorHandlerService
	{

		void HandleError(object source, string message);

		void HandleError(object source, Exception ex);

	}
}

