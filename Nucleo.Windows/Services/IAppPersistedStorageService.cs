﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Nucleo.Services
{
	public interface IAppPersistedStorageService : IService 
	{

		T GetObject<T>(string name);

		string GetValue(string name);

		void SetObject<T> (string name, T value);

		void SetValue(string name, string value);

	}
}
