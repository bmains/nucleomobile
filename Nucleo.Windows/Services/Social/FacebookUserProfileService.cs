using System;
using System.Collections.Generic;
using Facebook;


namespace Nucleo.Services.Social
{
	[ServiceImplementation]
	public class FacebookUserProfileService : IFacebookUserProfileService
	{


		public FacebookUserProfile GetInformationForCurrentUser (string token, string[] facebookFieldsToInclude)
		{
			var fb = new FacebookClient (token);
			var attributes = fb.Get<IDictionary<string, object>>("/me?fields=id,first_name,last_name," + String.Join(",", facebookFieldsToInclude));

			string firstName, lastName, id;

			firstName = (attributes["first_name"] as string) ?? "";
			lastName = (attributes["last_name"] as string) ?? "";
			id = (attributes["id"] as string) ?? "";
			IDictionary<string, object> attribs = null;

			if (facebookFieldsToInclude.Length > 0) {
				attribs = new Dictionary<string, object> ();

				foreach (var field in facebookFieldsToInclude) {
					attribs.Add (field, ((attributes [field] as string) ?? ""));
				}
			}

			return new FacebookUserProfile
			{ 
				FirstName = firstName, 
				LastName = lastName, 
				ID = id,
				Attributes = attribs
			};
		}

	}
}

