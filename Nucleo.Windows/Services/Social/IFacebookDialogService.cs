using System;


namespace Nucleo.Services.Social
{
	public interface IFacebookDialogService
	{

		FacebookRequestParameters ExtractRequestParameters(string uri);

		string GetDialogUrl(FacebookDialogOptions options);

	}


	public class FacebookDialogOptions
	{

		public string AppID { get; set; }

		public string Message { get; set; }

		public string ReturnUrl { get; set; }

		public string ToFacebookID { get; set; }

	}

	public class FacebookRequestParameters
	{

		public string RequestID { get; set; }

		public string To { get; set; }

	}
}

