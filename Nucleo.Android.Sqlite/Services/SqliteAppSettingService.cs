using System;
using SQLite;


namespace Nucleo.Services
{
	public class SqliteAppSettingService : IAppSettingService
	{
		private ISqliteDatabaseService _db = null;



		public SqliteAppSettingService (ISqliteDatabaseService db)
		{
			_db = db;
		}


		public string GetSetting (string name)
		{
			using (var conn = _db.OpenConnection ()) {
				var appSetting = conn.Table<Data.AppSetting> ().Where (i => i.Name == name).FirstOrDefault ();
				return (appSetting != null) ? appSetting.Value : null;
			}
		}

		public void SetSetting (string name, string value)
		{
			var appSetting = new Data.AppSetting { Name = name, Value = value };

			using (var conn = _db.OpenConnection ()) {
				conn.InsertOrReplace (appSetting);
			}
		}

	}
}

