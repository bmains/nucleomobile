using System;
using SQLite;


namespace Nucleo.Services
{
	public interface ISqliteDatabaseService
	{

		string GetDefaultDatabasePath();

		SQLiteConnection OpenConnection();

	}
}

