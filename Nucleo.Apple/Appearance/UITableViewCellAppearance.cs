using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;


namespace Nucleo.Appearance
{
	public static class UITableViewCellAppearance
	{

		public static UIColor BackgroundColor
		{
			get;
			set;
		}

		public static UIColor TextColor
		{
			get;
			set;
		}

		public static UIColor HighlightTextColor
		{
			get;
			set;
		}

		public static UIColor DetailTextColor
		{
			get;
			set;
		}

		public static UIColor HighlightDetailTextColor 
		{
			get;
			set;
		}

	}
}

