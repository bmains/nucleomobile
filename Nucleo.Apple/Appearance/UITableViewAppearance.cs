using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;


namespace Nucleo.Appearance
{
	public static class UITableViewAppearance
	{

		public static UIColor BottomRoundedHeaderColor {
			get;
			set;
		}

		public static UIColor TopRoundedHeaderColor {
			get;
			set;
		}

		public static UIColor ViewBackgroundColor {
			get;
			set;
		}


	}
}

