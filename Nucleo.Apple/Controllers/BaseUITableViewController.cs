using System;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.CoreAnimation;

using Nucleo.Appearance;


namespace Nucleo.Controllers
{
	public class BaseUITableViewController : UITableViewController
	{
		public BaseUITableViewController() { }

		public BaseUITableViewController (UITableViewStyle style)
			: base(style)
		{
		}

		public BaseUITableViewController(IntPtr ptr)
			: base(ptr)
		{
		}



		public override void ViewDidLoad ()
		{
			if (UITableViewAppearance.ViewBackgroundColor != null)
				this.View.BackgroundColor = UITableViewAppearance.ViewBackgroundColor;

			this.LoadDefaults ();

			base.ViewDidLoad ();
		}




		protected virtual void LoadDefaults()
		{

		}

	}
}

