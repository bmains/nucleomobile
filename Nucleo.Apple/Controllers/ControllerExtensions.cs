using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;


namespace Nucleo.Controllers
{
	public static class ControllerExtensions
	{

		public static void AddViewControllerToView(this UIViewController parentController, UIViewController childController, UIView parentView)
		{
			parentController.AddChildViewController (childController);
			childController.View.Frame = parentView.Frame;
			parentController.View.AddSubview (childController.View);
			childController.DidMoveToParentViewController (parentController);
		}

		public static void RemoveViewControllerFromView(this UIViewController childController)
		{
			childController.WillMoveToParentViewController (null);
			childController.View.RemoveFromSuperview ();
			childController.RemoveFromParentViewController ();
		}

	}
}

