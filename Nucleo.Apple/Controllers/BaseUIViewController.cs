using System;
using MonoTouch.UIKit;

using Nucleo.Services;
using Nucleo.Appearance;


namespace Nucleo.Controllers
{
	public abstract class BaseUIViewController : UIViewController
	{

		public BaseUIViewController ()
		{
		}

		public BaseUIViewController(IntPtr ptr)
			: base(ptr)
		{
		}

		public BaseUIViewController(string nib, MonoTouch.Foundation.NSBundle bundle)
			: base(nib, bundle)
		{
		}



		public override void ViewDidLoad ()
		{
			if (UIViewAppearance.BackgroundColor != null)
				this.View.BackgroundColor = UIViewAppearance.BackgroundColor;

			this.LoadDefaults ();

			base.ViewDidLoad ();


		}



		protected virtual void LoadDefaults()
		{

		}

	}
}

