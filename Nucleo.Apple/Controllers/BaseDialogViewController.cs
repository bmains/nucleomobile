using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.Dialog;


namespace Nucleo.Controllers
{
	public class BaseDialogViewController : DialogViewController
	{


		public BaseDialogViewController()
			:base(null)
		{
		}

		public BaseDialogViewController(UITableViewStyle style)
			: base(style, null)
		{
		}

		public BaseDialogViewController(UITableViewStyle style, bool pushing)
			: base(style, null, pushing)
		{
		}

		public BaseDialogViewController(UITableViewStyle style, RootElement root)
			: base(style, root)
		{
		}

		public BaseDialogViewController(UITableViewStyle style, RootElement root, bool pushing)
			: base(style, root, pushing)
		{
		}


		public override void ViewDidLoad ()
		{
			this.LoadDefaults ();
			this.LoadStyles ();

			base.ViewDidLoad ();
		}




		protected virtual void LoadDefaults()
		{

		}

		protected virtual void LoadStyles()
		{

		}
	}
}

