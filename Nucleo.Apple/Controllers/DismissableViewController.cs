using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using Nucleo.Services;


namespace Nucleo.Controllers
{
	//From https://coderwall.com/p/lc4kja
	public class DismissableModalController : BaseUIViewController
	{
		protected UITapGestureRecognizer _dismissRecognizer;



		public DismissableModalController ()
		{
			_dismissRecognizer = new UITapGestureRecognizer (OnTapOutside);
			_dismissRecognizer.NumberOfTapsRequired = 1u;
			_dismissRecognizer.CancelsTouchesInView = false;
		}



		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			View.Window.AddGestureRecognizer (_dismissRecognizer);

			View.SizeToFit ();
		}

		private void OnTapOutside(UITapGestureRecognizer recogniser)
		{
			if (recogniser.State == UIGestureRecognizerState.Ended)
			{
				var window = View.Window;
				DismissViewController (true, () => window.RemoveGestureRecognizer (_dismissRecognizer));
			}
		}

	}
}