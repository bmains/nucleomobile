using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

namespace Nucleo.Controllers
{
	public partial class BrowserController : BaseUIViewController
	{
		private UIWebView _browser = null;
		private NSUrlRequest _request = null;



		public BrowserDelegate Delegate {
			get;
			set;
		}

		public NSUrlRequest Request
		{
			get { return _request; }
		}



		public BrowserController () : base ()
		{

		}



		public void Reset()
		{
			if (_browser == null)
				return;

			_browser.LoadRequest (new NSUrlRequest (new NSUrl ("about:blank")));
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			_browser = this.CreateWebView ();
			this.View.AddSubview (_browser);
		
			_browser.LoadError += HandleLoadError;
			_browser.LoadFinished += HandleLoadFinished;
			_browser.LoadStarted += HandleLoadStarted;

			//If load request was called before load, then navigate to now
			if (_request != null)
				_browser.LoadRequest (_request);
		}

		void HandleLoadError (object sender, UIWebErrorArgs e)
		{
			if (this.Delegate != null)
				this.Delegate.RequestErrored (this.Request, e.Error);
		}

		void HandleLoadFinished (object sender, EventArgs e)
		{
			if (this.Delegate != null)
				this.Delegate.RequestCompleted (this.Request);
		}

		void HandleLoadStarted (object sender, EventArgs e)
		{
			if (this.Delegate != null)
				this.Delegate.RequestStarted (this.Request);
		}



		protected virtual UIWebView CreateWebView()
		{
			return new UIWebView (this.View.Frame);
		}

		public void LoadRequest(string url)
		{
			_request = new NSUrlRequest (new NSUrl (url));
			if (_browser != null)
				_browser.LoadRequest (_request);
		}

	}


	public class BrowserDelegate
	{

		public virtual void RequestErrored (NSUrlRequest request, NSError error) 
		{
		}

		public virtual void RequestStarted(NSUrlRequest request)
		{
		}

		public virtual void RequestCompleted(NSUrlRequest request)
		{
		}

	}
}
