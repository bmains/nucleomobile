using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.MapKit;
using MonoTouch.CoreLocation;


namespace Nucleo.MapKit
{
	public class MapAnnotation : MKAnnotation
	{
		string _title, _subtitle;



		public override CLLocationCoordinate2D Coordinate 
		{
			get;
			set;
		}

		public override string Subtitle 
		{ 
			get
			{ 
				return _subtitle;
			}
		}

		public override string Title 
		{ 
			get
			{ 
				return _title;
			}
		}
	


		public MapAnnotation (CLLocationCoordinate2D coordinate, string title, string subtitle) {
			this.Coordinate = coordinate;
			this._title = title;
			this._subtitle = subtitle;
		}
	}
}

