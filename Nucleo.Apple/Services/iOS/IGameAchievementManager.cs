using System;

namespace Nucleo.Services.iOS
{
	public interface IGameAchievementManager
	{
		GameAchievementResults TransmitAchievement(string achievement, double percentComplete);
	}


	public class GameAchievementResults
	{

		public Exception Exception { get; set; }

		public bool Success { get; set; }

	}
}

