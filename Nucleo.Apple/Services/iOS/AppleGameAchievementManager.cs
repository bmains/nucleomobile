using System;
using System.IO;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.GameKit;
using MonoTouch.UIKit;


namespace Nucleo.Services.iOS
{
	public class AppleGameAchievementManager : IGameAchievementManager
	{

		public GameAchievementResults ResetAchievements()
		{
			NSError error = null;

			GKAchievement.ResetAchivements(new GKNotificationHandler((err) =>
				{
					error = err;
				}));

			return new GameAchievementResults
			{
				Success = (error == null),
				Exception = (error != null) ? new Exception(error.Description) : null
			};
		}

		public GameAchievementResults TransmitAchievement(string achievement, double percentComplete)
		{
			GKAchievement gkAchievement = new GKAchievement (achievement);
			gkAchievement.PercentComplete = percentComplete;

			NSError error = null;

			gkAchievement.ReportAchievement (new GKNotificationHandler ((err) => {
				error = err;
			}));

			return new GameAchievementResults
			{
				Success = (error == null),
				Exception = (error != null) ? new Exception(error.Description) : null
			};
		}
	}
}

