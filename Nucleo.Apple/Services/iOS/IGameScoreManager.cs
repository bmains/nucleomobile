using System;


namespace Nucleo.Services.iOS
{
	public interface IGameScoreManager
	{
		GameScoreResult TransmitScore(long score, GameScoreOptions options);
	}


	public class GameScoreOptions
	{

		public ulong Context { get; set; }

		public string Leaderboard { get; set; }

		public bool ShouldSetDefaultLeaderboard { get; set; }



		public GameScoreOptions()
		{
			ShouldSetDefaultLeaderboard = true;
		}

	}

	public class GameScoreResult
	{
		public Exception Error { get; set; }

		public bool Success { get; set; }
	}
}

