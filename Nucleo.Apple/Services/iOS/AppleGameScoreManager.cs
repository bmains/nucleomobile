using System;
using System.IO;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.GameKit;
using MonoTouch.UIKit;


namespace Nucleo.Services.iOS
{
	public class AppleGameScoreManager : IGameScoreManager
	{

		public AppleGameScoreManager()
		{

		}



		public GameScoreResult TransmitScore(long score, GameScoreOptions options)
		{
			GKScore appleScore = !string.IsNullOrEmpty(options.Leaderboard)
			                ? new GKScore(options.Leaderboard)
			                : new GKScore();

			if (options != null)
				appleScore.Context = options.Context;

			appleScore.Value = score;
			appleScore.ShouldSetDefaultLeaderboard = (options != null) ? options.ShouldSetDefaultLeaderboard : true;

			NSError error = null;

			appleScore.ReportScore(new GKNotificationHandler((err) =>
				{
					error = err;
				}));


			return new GameScoreResult 
			{ 
				Success = (error == null), 
				Error = (error != null) ? new Exception(error.Description) : null
			};
		}

	}
}

