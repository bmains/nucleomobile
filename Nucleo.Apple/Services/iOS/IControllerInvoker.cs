using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;


namespace Nucleo.Services.iOS
{
	public interface IControllerInvoker
	{

		T Create<T> ()
			where T:UIViewController;

	}
}

