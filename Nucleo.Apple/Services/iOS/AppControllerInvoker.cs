using System;

using Nucleo.Services;


namespace Nucleo.Services.iOS
{
	[ServiceImplementation]
	public class AppControllerInvoker : IControllerInvoker
	{
		private IDependencyResolver _resolver = null;



		public AppControllerInvoker (IDependencyResolver resolver)
		{
			_resolver = resolver;
		}



		public T Create<T> () where T : MonoTouch.UIKit.UIViewController
		{
			try
			{
				if (_resolver != null)
					return _resolver.GetInstance<T> ();
				else
					return Activator.CreateInstance<T>();
			}
			catch(Exception ex) {
				Exception baseEx = ex.InnerException;

				return null;
			}
		}

	}
}

