using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;


namespace Nucleo.Services.UI
{
	[ServiceImplementation]
	public class AppleMessageDialogService : IMessageDialogService
	{
		public AppleMessageDialogService ()
		{
		}



		public void Show (object source, string title, string message)
		{
			new UIAlertView (title, message, null, "OK").Show ();
		}

	}
}

