using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;


namespace Nucleo.Events
{
	public static class UIViewControllerExtensions
	{

		private static void RaiseForController(UIViewController controller, string eventName, IDictionary<string, object> args)
		{
			//TODO:find method with matching delegate
			var methods = controller.GetType ().GetMethods (BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			              
			foreach (var method in methods) {
				var attribs = method.GetCustomAttributes (typeof(EventHandleAttribute), true);

				if (attribs != null && attribs.Length > 0 && String.Compare(((EventHandleAttribute)attribs [0]).EventName, eventName, true) == 0) {
					method.Invoke (controller, 
						new object[] 
						{ 
							controller, 
							new ControllerEventEventArgs  { 
								EventName = eventName, 
								Arguments = args 
							}
						}
					);
				}
			}


			foreach (var childController in controller.ChildViewControllers) {
				RaiseForController (childController, eventName, args);
			}
		}

		public static void RaiseEvent(this UIViewController controller, string eventName, IDictionary<string, object> args)
		{
			var navController = controller.NavigationController;
			var ctlr = controller;

			while (ctlr != null && navController == null) {
				ctlr = ctlr.ParentViewController;
				if (ctlr != null)
					navController = ctlr.NavigationController;
			}

			if (navController != null) {
				foreach (var childController in navController.ChildViewControllers) {
					RaiseForController (childController, eventName, args);
				}
			} 
		}

	}
}

