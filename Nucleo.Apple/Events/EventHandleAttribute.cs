using System;


namespace Nucleo.Events
{
	[AttributeUsage(AttributeTargets.Method)]
	public class EventHandleAttribute : Attribute
	{
		public string EventName { get; set; }



		public EventHandleAttribute (string eventName)
		{
			if (string.IsNullOrEmpty (eventName))
				throw new ArgumentNullException ("eventName");

			this.EventName = eventName;
		}
	}
}

