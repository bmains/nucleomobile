using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;


namespace Nucleo.Events
{
	public class ControllerEventEventArgs
	{

		public IDictionary<string, object> Arguments { get; set; }

		public string EventName { get; set; }

	}
}

