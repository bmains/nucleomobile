using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;


namespace Nucleo.UI
{
	public class AlertDialogDelegate : UIAlertViewDelegate
	{

		public event AlertClickEventHandler AlertClicked;



		public AlertDialogDelegate ()
		{
		}


		public override void Clicked (UIAlertView alertview, int buttonIndex)
		{
			this.OnAlertClicked (new AlertClickEventArgs { Alert = alertview, ButtonIndex = buttonIndex });
		}

		protected virtual void OnAlertClicked(AlertClickEventArgs e)
		{
			if (AlertClicked != null)
				AlertClicked (this, e);
		}
	}


	public class AlertClickEventArgs
	{
		public UIAlertView Alert { get; set; }

		public int ButtonIndex { get; set; }
	}

	public delegate void AlertClickEventHandler(object sender, AlertClickEventArgs e);
}

