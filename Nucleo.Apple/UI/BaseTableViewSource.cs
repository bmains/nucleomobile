using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.CoreAnimation;
using System.Collections.Specialized;
using System.Windows.Input;


namespace Nucleo.UI
{
	public class TableConstants
	{
		public const string EMPTY_CELL_IDENTIFIER = "__NoItems";
	}

	public abstract class BaseTableViewSource<T> : UITableViewSource
		where T: class
	{
		private IList<T> _data = null;
		private IDictionary<string, IList<T>> _groupedData = null;



		public event TableViewCellSelectEventHandler<T> AccessorySelected;

		public event TableViewCellSelectEventHandler<T> CellSelected;

		public event NotifyCollectionChangedEventHandler CollectionChanged;



		public ICommand AccessoryTappedCommand { get; set; }

		public ICommand CollectionChangeCommand { get; set; }

		public IList<T> Data { 
			get { return _data; }
			set { 
				_data = value;

				if (_data is INotifyCollectionChanged) {
					((INotifyCollectionChanged)_data).CollectionChanged += HandleDataChanged;
				}
			}
		}

		public bool DisplayNoItemsMessage { get; set; }

		public string NoItemsMessage { get; set; }

		public IDictionary<string, IList<T>> GroupedData { 
			get { return _groupedData; }
			set {
				_groupedData = value;

				if (_groupedData is INotifyCollectionChanged)
					((INotifyCollectionChanged)_groupedData).CollectionChanged += HandleGroupDataChange;
			}
		}

		protected bool IsGrouped
		{
			get { return this.GroupedData != null; }
		}

		public ICommand SelectedCommand { get; set; }




		public override void AccessoryButtonTapped (UITableView tableView, NSIndexPath indexPath)
		{
			var dataItem = this.GetDataItem (indexPath);

			if (this.AccessorySelected != null) {
				this.AccessorySelected (this, new TableViewCellSelectEventArgs<T> {
					Cell = this.GetCell(tableView, indexPath),
					DataItem = dataItem,
					IndexPath = indexPath
				});
			}

			if (this.AccessoryTappedCommand != null) {
				if (this.AccessoryTappedCommand.CanExecute (dataItem))
					this.AccessoryTappedCommand.Execute (dataItem);
			}
		}

		public virtual UITableViewCell CreateEmptyCell()
		{
			var cell = new UITableViewCell (UITableViewCellStyle.Default, TableConstants.EMPTY_CELL_IDENTIFIER);
			cell.UserInteractionEnabled = false;
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;

			return cell;
		}

		public virtual UITableViewCell CreateNewCell(NSIndexPath path)
		{
			return new UITableViewCell (this.GetStyle(), this.GetCellIdentifier ()) { Accessory = this.GetAccessory() };
		}

		public virtual void DequeueEmptyCell (UITableView tableView)
		{
			tableView.DequeueReusableCell (TableConstants.EMPTY_CELL_IDENTIFIER);
		}

		protected override void Dispose (bool disposing)
		{
			if (this.IsGrouped) {
				if (this.GroupedData != null && this.GroupedData is INotifyCollectionChanged) {
					((INotifyCollectionChanged)this.GroupedData).CollectionChanged -= HandleGroupDataChange;
				}
			} 
			else {
				if (this.Data != null && this.Data is INotifyCollectionChanged) {
					((INotifyCollectionChanged)this.Data).CollectionChanged -= HandleGroupDataChange;
				}
			}

			base.Dispose (disposing);
		}

		protected virtual UITableViewCellAccessory GetAccessory()
		{
			return UITableViewCellAccessory.None;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell;

			if (this.HasNoItems (indexPath) && this.DisplayNoItemsMessage == true) {
				cell = tableView.DequeueReusableCell (TableConstants.EMPTY_CELL_IDENTIFIER);
				if (cell == null)
					cell = this.CreateEmptyCell ();

				cell.TextLabel.Text = (this.NoItemsMessage ?? "(No Items)");
				return cell;
			} 
			else if (indexPath.Section <= 0 && indexPath.Row == 0) {
				this.DequeueEmptyCell(tableView);
			}

			cell = tableView.DequeueReusableCell (this.GetCellIdentifier ());
			if (cell == null)
				cell = this.CreateNewCell (indexPath);
				
			//If no items, one item is returned when an empty message is displayed
			this.PopulateCell (
				cell,
				this.GetDataItem (indexPath),
				indexPath);

			return cell;
		}

		/// <summary>
		/// Gets the unique identifier for the cell.  Returns the underlying data type name by default.
		/// </summary>
		/// <returns>The cell identifier.</returns>
		protected virtual string GetCellIdentifier ()
		{
			return typeof(T).Name;
		}

		/// <summary>
		/// Gets the data item associated to an index path.
		/// </summary>
		/// <returns>The data item.</returns>
		/// <param name="path">The index path to the data item.</param>
		protected T GetDataItem(NSIndexPath path)
		{
			if (this.IsGrouped == true) {
				var group = this.GroupedData[this.SectionFor (path.Section)];
				if (group != null && group.Count >= (path.Row + 1))
					return (T)group [path.Row];
			} 
			else if (this.Data != null) {
				if (this.Data.Count >= (path.Row + 1))
					return this.Data [path.Row];
			} 

			return null;
		}

		/// <summary>
		/// Gets the collection of items associated to the group, if the group is found.  If not found, or not in grouped mode, null is returned.
		/// </summary>
		/// <returns>The group data or null if not grouped/not found.</returns>
		/// <param name="name">The name of the group.</param>
		public IList<T> GetGroupData(string name)
		{
			if (this.IsGrouped == false)
				return null;

			if (!this.GroupedData.ContainsKey (name))
				return null;

			return this.GroupedData [name];
		}

		/// <summary>
		/// Gets the style of the table.  Default by default.
		/// </summary>
		/// <returns>The style.</returns>
		protected virtual UITableViewCellStyle GetStyle ()
		{
			return UITableViewCellStyle.Default;
		}

		/// <summary>
		/// Creates and loads into the table footer a view that displays a rounded corner.
		/// </summary>
		/// <returns>The rounded footer view.</returns>
		/// <param name="bounds">The bounds to render in.</param>
		/// <param name="roundingSize">The rounding size.</param>
		protected UIView LoadRoundedFooterView(UITableView tableView, RectangleF bounds, SizeF roundingSize)
		{
			var color = Appearance.UITableViewAppearance.BottomRoundedHeaderColor;
			if (color == null)
				color = UIColor.Black;

			var view = new UIView ();
			view.BackgroundColor = color;
			view.Bounds = bounds;
			view.Frame = bounds;

			UIBezierPath path = UIBezierPath.FromRoundedRect (view.Bounds, UIRectCorner.BottomLeft | UIRectCorner.BottomRight, roundingSize);
			CAShapeLayer layer = new CAShapeLayer ();
			layer.Frame = tableView.TableFooterView.Bounds;
			layer.Path = path.CGPath;
			view.Layer.Mask = layer;

			return view;
		}

		/// <summary>
		/// Creates and loads into the table header a view that displays a rounded corner.
		/// </summary>
		/// <returns>The rounded header view.</returns>
		/// <param name="bounds">The bounds to render in.</param>
		/// <param name="roundingSize">The rounding size.</param>
		protected UIView LoadRoundedHeaderView(UITableView tableView, RectangleF bounds, SizeF roundingSize)
		{
			var color = Appearance.UITableViewAppearance.TopRoundedHeaderColor;
			if (color == null)
				color = UIColor.Black;

			var view = new UIView ();
			view.BackgroundColor = color;
			view.Bounds = bounds;
			view.Frame = bounds;

			UIBezierPath path = UIBezierPath.FromRoundedRect (view.Bounds, UIRectCorner.TopLeft | UIRectCorner.TopRight, roundingSize);
			CAShapeLayer layer = new CAShapeLayer ();
			layer.Frame = tableView.TableHeaderView.Bounds;
			layer.Path = path.CGPath;
			view.Layer.Mask = layer;

			return view;
		}

		protected bool HasNoItems(NSIndexPath path)
		{
			if (this.IsGrouped) {
				return this.GroupedData.Count == 0;
			} 
			else {
				return this.Data == null || this.Data.Count == 0;
			}
		}

		public override int NumberOfSections (UITableView tableView)
		{
			if (this.IsGrouped == true) {
				var count = this.GroupedData.Keys.Count ();
				if (count > 0)
					return count;

				//Need to show one group for the empty item
				if (this.DisplayNoItemsMessage)
					return 1;
				else
					return 0;
			} 
			else {
				return 1;
			}
		}

		protected abstract void PopulateCell (UITableViewCell cell, T dataItem, NSIndexPath path);

		public bool RemoveFromGroup(string name, T dataItem)
		{
			if (!this.IsGrouped)
				return false;

			if (!this.GroupedData.ContainsKey (name))
				return false;

			var list = this.GroupedData [name];
			list.Remove (dataItem);

			if (list.Count > 0)
				this.GroupedData [name] = list;
			else
				this.GroupedData.Remove (name);

			return true;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			var dataItem = this.GetDataItem (indexPath);
			var cell = this.GetCell (tableView, indexPath);

			if (this.CellSelected != null) {
				this.CellSelected (this, new TableViewCellSelectEventArgs<T> {
					Cell = cell,
					DataItem = dataItem,
					IndexPath = indexPath
				});
			}

			if (this.SelectedCommand != null) {
				if (this.SelectedCommand.CanExecute (dataItem))
					this.SelectedCommand.Execute (dataItem);
			}
		}

		public override int RowsInSection (UITableView tableView, int section)
		{
			if (this.IsGrouped == true) {
				var name = this.SectionFor(section);

				if (string.IsNullOrEmpty (name) && !this.GroupedData.ContainsKey(name)) {
					return this.DisplayNoItemsMessage ? 1 : 0;
				}

				var data = this.GroupedData [name];
				if (data != null && data.Count > 0)
					return data.Count;
			}
			else if (this.Data != null && this.Data.Count > 0)
				return this.Data.Count;

			//At this point, we know we have 0 records
			//if no items message, then 
			if (this.DisplayNoItemsMessage)
				return 1;
			else
				return 0;
		}

		public string SectionFor(int section)
		{
			if (section <= this.GroupedData.Keys.Count - 1)
				return this.GroupedData.Keys.ElementAt (section);

			return "";
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			if (this.IsGrouped) {
				return this.SectionFor (section);
			} 
			else
				return null;
		}

		private void FireChangeCommand(NotifyCollectionChangedEventArgs e)
		{
			if (CollectionChangeCommand == null)
				return;

			if (CollectionChangeCommand.CanExecute (e))
				CollectionChangeCommand.Execute (e);
		}
		

		void HandleGroupDataChange (object sender, NotifyCollectionChangedEventArgs e)
		{
			if (CollectionChanged != null)
				CollectionChanged (this, e);
		}

		void HandleDataChanged (object sender, NotifyCollectionChangedEventArgs e)
		{
			if (CollectionChanged != null)
				CollectionChanged (this, e);
		}
	}
}
