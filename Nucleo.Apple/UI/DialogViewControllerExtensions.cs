using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;


namespace Nucleo.UI
{
	public static class DialogViewControllerExtensions
	{

		public static T CreateNewObject<T>(this DialogViewController dialogController)
		{
			var obj = Activator.CreateInstance<T> ();
			PopulateObject (dialogController, obj);

			return obj;
		}

		public static void UpdateExistingObject<T>(this DialogViewController dialogController, T obj)
		{
			PopulateObject (dialogController, obj);
		}


		private static void PopulateObject<T>(DialogViewController dialogController, T obj)
		{
			var type = typeof(T);

			foreach (var section in dialogController.Root) {
				foreach (var item in section) {

					if (item is Nucleo.UI.BusinessEntryElement) {
						var field = (Nucleo.UI.BusinessEntryElement)item;
						var prop = type.GetProperty (field.FieldName);

						prop.SetValue (dialogController.Root, field.Value, null);
					}

				}
			}
		}

	}
}

