using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;


namespace Nucleo.UI
{
	public class TableViewCellGenericSelectEventArgs<T> : EventArgs
		where T: class
	{

		public UITableViewCell Cell { get; set; }

		public T DataItem { get; set; }

		public NSIndexPath IndexPath { get; set; }

	}

	public delegate void TableViewCellGenericSelectEventHandler<T>(object sender, TableViewCellGenericSelectEventArgs<T> e)
		where T: class;
}

