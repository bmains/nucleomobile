using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Nucleo.Appearance;
using A = Nucleo.Appearance;


namespace Nucleo.UI
{
	/// <summary>
	/// Represents a table view cell; can use this cell type, or inherit from it.  You must call <see cref="Initialize"/> in order to get the benefits of automatic cell initialization. 
	/// </summary>
	public class TableViewCell : UITableViewCell
	{
		public TableViewCell ()
			: base()
		{
		}

		public TableViewCell(RectangleF frame)
			: base(frame)
		{
		}

		public TableViewCell(UITableViewCellStyle style, string reuseIdentifier)
			: base(style, reuseIdentifier)
		{
		}



		public virtual void Initialize()
		{
			if (A.UITableViewCellAppearance.BackgroundColor != null)
				this.BackgroundColor = A.UITableViewCellAppearance.BackgroundColor;
			if (A.UITableViewCellAppearance.TextColor != null)
				this.TextLabel.TextColor = A.UITableViewCellAppearance.TextColor;
			if (A.UITableViewCellAppearance.HighlightTextColor != null)
				this.TextLabel.HighlightedTextColor = A.UITableViewCellAppearance.HighlightTextColor;
			if (A.UITableViewCellAppearance.DetailTextColor != null)
				this.DetailTextLabel.TextColor = A.UITableViewCellAppearance.DetailTextColor;
			if (A.UITableViewCellAppearance.HighlightDetailTextColor != null)
				this.DetailTextLabel.HighlightedTextColor = A.UITableViewCellAppearance.HighlightDetailTextColor;
		}

	}
}

