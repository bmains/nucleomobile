using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;


namespace Nucleo.UI
{
	public class EmptyTableCell : UITableViewCell
	{

		public EmptyTableCell()
			: base(UITableViewCellStyle.Default, "Empty")
		{
			this.Initialize ();
		}

		public EmptyTableCell(RectangleF frame)
			: base(frame) 
		{
			this.Initialize ();
		}

		public EmptyTableCell(UITableViewCellStyle style, string reuseIdentifier)
			: base(style, reuseIdentifier)
		{
			this.Initialize ();
		}



		protected void Initialize()
		{
			this.SelectionStyle = UITableViewCellSelectionStyle.None;
		}

	}
}

