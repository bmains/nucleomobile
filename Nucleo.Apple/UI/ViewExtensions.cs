using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreAnimation;


namespace Nucleo.UI
{
	public static class ViewExtensions
	{

		public static void ResizeView(this UIView contentWindow, UIScrollView scrollWindow)
		{
			var size = contentWindow.Bounds.Size;
			contentWindow.Frame = new RectangleF (0, 0, size.Width, size.Height);

			scrollWindow.Add (contentWindow);
			scrollWindow.ContentSize = size;
		}

	}
}

