using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;


namespace Nucleo.UI
{
	public class BusinessEntryElement : EntryElement
	{
		public BusinessEntryElement (string name, string fieldName)
			: this(name, "", "", fieldName)
		{
			this.FieldName = fieldName;
		}

		public BusinessEntryElement (string name, string placeholder, string value, string fieldName)
			: base(name, placeholder, value)
		{
			this.FieldName = fieldName;
		}

		public BusinessEntryElement (string name, string placeholder, string value, bool isPassword, string fieldName)
			: base(name, placeholder, value, isPassword)
		{
			this.FieldName = fieldName;
		}



		public string FieldName
		{
			get;
			private set;
		}

	}
}

