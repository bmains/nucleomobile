using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;


namespace Nucleo.UI
{
	public class TableViewCellSelectEventArgs<T> : EventArgs
	{

		public UITableViewCell Cell { get; set; }

		public T DataItem { get; set; }

		public NSIndexPath IndexPath { get; set; }

	}

	public delegate void TableViewCellSelectEventHandler<T>(object sender, TableViewCellSelectEventArgs<T> e);
	
}

