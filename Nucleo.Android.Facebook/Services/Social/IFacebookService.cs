using System;
using System.Collections.Generic;


namespace Nucleo.Services.Social
{
	public interface IFacebookService
	{

		string GetLoginUrl(string appID, string extendedPermissions);

		string GetLoginUrl(string appID, string redirectUri, string extendedPermissions);

	}
	

}

