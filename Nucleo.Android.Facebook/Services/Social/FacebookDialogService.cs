using System;


namespace Nucleo.Services.Social
{
	[ServiceImplementation]
	public class FacebookDialogService : IFacebookDialogService
	{

		public FacebookRequestParameters ExtractRequestParameters (string uri)
		{
			string request = this.GetParamValue (uri, "request");
			string to = this.GetParamValue (uri, "to");

			return new FacebookRequestParameters { RequestID = request, To = to };
		}

		protected virtual string GetBaseFacebookUrl()
		{
			return "https://www.facebook.com/dialog/apprequests";
		}

		public string GetDialogUrl (FacebookDialogOptions options)
		{
			if (options == null)
				throw new ArgumentNullException ("options");
			if (string.IsNullOrWhiteSpace (options.AppID))
				throw new ArgumentNullException ("options.AppID");
			if (string.IsNullOrWhiteSpace(options.Message))
				throw new ArgumentNullException ("options.Message");
			//Default the return URL if not set
			if (string.IsNullOrWhiteSpace(options.ReturnUrl))
				options.ReturnUrl = "https://www.facebook.com/connect/login_success.html";

			return this.GetBaseFacebookUrl() + "?" + 
				"app_id=" + options.AppID +
					"&message=" + this.GetMessage(options) + 
					(!string.IsNullOrEmpty(options.ToFacebookID) ? "&to=" + options.ToFacebookID : "") +
					"&redirect_uri=" + options.ReturnUrl;
		}

		private string GetMessage(FacebookDialogOptions options)
		{
			if (string.IsNullOrWhiteSpace (options.Message))
				return "";

			return options.Message.Replace ("&", "%26").Replace("\"", "%22").Replace(" ", "%20");
		}

		private string GetParamValue(string uri, string name)
		{
			int pos = uri.IndexOf (name) + name.Length + 2;
			string arg = "";

			for (var i = pos; i < uri.Length; i++) {
				if (uri [i] == '&') {
					break;
				}

				arg += uri[i];
			}

			return arg;
		}

	}
}

