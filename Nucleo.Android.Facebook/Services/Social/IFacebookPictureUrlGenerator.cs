using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Nucleo.Services.Social
{
	public interface IFacebookPictureUrlGenerator
	{

		string GetProfileUrl(string uid, int width, int height);

	}
}

