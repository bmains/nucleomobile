using System;
using System.Collections.Generic;
using Facebook;


namespace Nucleo.Services.Social
{
	[ServiceImplementation]
	public class FacebookService : IFacebookService
	{
		private const string SuccessPage = "https://www.facebook.com/connect/login_success.html";



		public string GetLoginUrl(string appID, string extendedPermissions)
		{
			return GetLoginUrl (appID, SuccessPage, extendedPermissions);
		}

		public string GetLoginUrl(string appID, string redirectUri, string extendedPermissions)
		{
			var parameters = new Dictionary<String, Object> ();

			parameters.Add("client_id", appID);
			parameters.Add ("redirect_uri", redirectUri);

			// The requested response: an access token (token), an authorization code (code), or both (code token).
			parameters.Add("response_type", "token");

			// list of additional display modes can be found at http://developers.facebook.com/docs/reference/dialogs/#display
			//parameters.Add("display", "touch");

			// add the 'scope' parameter only if we have extendedPermissions.
			if (!string.IsNullOrWhiteSpace(extendedPermissions))
				parameters.Add("scope", extendedPermissions);

			// generate the login url
			var fb = new FacebookClient();
			return fb.GetLoginUrl(parameters).ToString ();
		}

	}
}

