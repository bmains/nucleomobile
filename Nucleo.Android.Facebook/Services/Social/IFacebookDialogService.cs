using System;


namespace Nucleo.Services.Social
{
	public interface IFacebookDialogService
	{

		FacebookRequestParameters ExtractRequestParameters(string uri);

		string GetDialogUrl(FacebookDialogOptions options);

	}


	public class FacebookDialogOptions
	{

		/// <summary>
		/// Gets or sets the Facebook application ID.  This field is required.
		/// </summary>
		public string AppID { get; set; }

		/// <summary>
		/// Gets or sets the message to show the user.  This field is required.
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the URL to return to; this field is optional.  A default facebook URL is used as an alternative.
		/// </summary>
		public string ReturnUrl { get; set; }

		/// <summary>
		/// Gets or sets the ID of the other user to send the request to.  This is optional; if not provided, friends can be selected in the UI.
		/// </summary>
		public string ToFacebookID { get; set; }

	}

	public class FacebookRequestParameters
	{

		public string RequestID { get; set; }

		public string To { get; set; }

	}
}

