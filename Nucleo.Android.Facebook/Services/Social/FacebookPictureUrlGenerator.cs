using System;


namespace Nucleo.Services.Social
{
	[ServiceImplementation]
	public class FacebookPictureUrlGenerator : IFacebookPictureUrlGenerator
	{
		public string GetProfileUrl (string uid, int width, int height)
		{
			return "https://graph.facebook.com/" + uid + "/picture?width=" + width.ToString() + "&height=" + height.ToString();
		}
	}
}

