using System;
using System.Collections.Generic;
using Facebook;


namespace Nucleo.Services.Social
{
	public interface IFacebookUserProfileService
	{

		FacebookUserProfile GetInformationForCurrentUser(string token, string[] facebookFieldsToInclude);

	}

	public class FacebookUserProfile
	{

		public IDictionary<string, object> Attributes 
		{
			get; 
			set; 
		}

		public string ID { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }


	}

}

