using System;
using System.Collections.Generic;
using TinyIoC;


namespace Nucleo.Services
{
	public class TinyIOCDependencyResolver : IDependencyResolver
	{
		private TinyIoCContainer _container = null;



		public TinyIOCDependencyResolver(TinyIoCContainer container)
		{
			if (container == null)
				throw new ArgumentNullException ("container");

			_container = container;
		}




		public IEnumerable<TInt> GetAllInstances<TInt> ()
			where TInt: class
		{
			return _container.ResolveAll <TInt> ();
		}

		public IEnumerable<object> GetAllInstances (Type tint)
		{
			return _container.ResolveAll (tint);
		}

		public TInt GetInstance<TInt> ()
			where TInt: class
		{
			return _container.Resolve<TInt> ();
		}

		public object GetInstance (Type type)
		{
			return _container.Resolve (type);
		}

	}
}

